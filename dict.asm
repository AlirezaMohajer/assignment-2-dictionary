%include "lib.inc"
%define OFFSET 8  ;dict element size

global find_word

section .text

find_word:
    push r12
    push r13

    mov  r12, rdi
    mov  r13, rsi
    .iter:
        test rsi, rsi
        jz   .not_found
        add  rsi, OFFSET
        mov  rdi, r12
        call string_equals
        test rax, rax
        jnz .was_found
        mov rsi, [r13]
        mov r13, rsi
        jmp .iter

    .was_found:
        mov rax, r13
        jmp .return
    .not_found:
        xor rax, rax
    .return:
        pop r13
        pop r12
    ret

















