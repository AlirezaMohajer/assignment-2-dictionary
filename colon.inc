%define POINTER 0

%macro colon 2

   %ifid %2
      %2: dq POINTER 
      %define POINTER %2
   %else
      %error "first argument is not a string"
   %endif

   %ifstr %1
      db %1, 0
   %else
      %error "second argument is not a label"
   %endif
   
%endmacro
