%include "dict.inc"
%include "lib.inc"
%include "words.inc"


%define NEXT_INDEX 8
%define BUFFER_SIZE 256

section .rodata
not_found: db "Key was not found!", 0xA, 0
read_fail: db "Error reading stdin!", 0xA, 0

section .bss
buffer: resb BUFFER_SIZE

section .text

global _start 
_start:
   mov rdi, buffer
   mov rsi, BUFFER_SIZE
   call read_word
   test rax, rax
   jz .read_word_fail
   push rdx
   mov rdi, buffer
   mov rsi, POINTER
   call find_word
   pop rdx
   test rax, rax
   jz .not_found
   mov rdi, rax
   add rdi, NEXT_INDEX
   add rdi, rdx
   inc rdi
   call print_string
   jmp exit


   .not_found:
      mov rdi, not_found
      jmp .error_out

   .read_word_fail:
      mov rdi, read_fail

   .error_out:
      call print_error
      jmp exit
