%define NEW_LINE 10
%define SPACE 32    
%define TAB 9
%define BASE_OF_SYSTEM 10
%define READ_SYSCALL_NUMBER 0
%define WRITE_SYSCALL_NUMBER 1
%define EXIT_SYSCALL_NUMBER 60
%define STD_IN 0
%define STD_OUT 1
%define STD_ERR 2
%define MAX_DIGITS 20

global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error


section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL_NUMBER
    syscall
    


; Принимает указатель на нуль-терминированную строку, возвращает её длину 
string_length:
    xor rax, rax                
    .loop:                  
        cmp byte[rdi+rax], 0 
        je .end              
        inc rax              
        jmp .loop               
    .end:
        ret   


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi                    
    call string_length           
    pop rsi                     
    mov rdx, rax                
    mov rax, WRITE_SYSCALL_NUMBER                  
    mov rdi, STD_OUT               
    syscall 
    ret


; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax                
    push rdi                  
    mov rsi, rsp            
    mov rax, WRITE_SYSCALL_NUMBER         
    mov rdi, STD_OUT           
    mov rdx, 1            
    syscall
    pop rdi                 
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE       
    jmp print_char




; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    sub rsp, MAX_DIGITS + 1
    mov rax, rdi
    mov rcx, MAX_DIGITS
    mov r9, BASE_OF_SYSTEM
    mov byte[rsp + MAX_DIGITS], 0

.loop:
    xor rdx, rdx
    div r9
    add dl, '0'
    dec rcx
    mov [rsp + rcx], dl
    test rax, rax
    jne .loop
.print:
    mov rdi, rsp
    add rdi, rcx
    call print_string
add rsp, MAX_DIGITS + 1
ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .positive
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.positive:
    jmp print_uint
                


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:                 
    xor r8, r8                 
    xor r9, r9
    xor rcx, rcx                
    .loop:          
        mov r8b, byte[rdi+rcx] 
        mov r9b, byte[rsi+rcx] 
        cmp r8, r9             
        jne .not_equal         
        cmp r8, 0               
        je .equal              
        inc rcx               
        jmp .loop              
    .equal:                 
        mov rax, 1
        ret
    .not_equal:
        mov rax, 0
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, READ_SYSCALL_NUMBER              
    mov rdx, 1                  
    mov rdi, STD_IN             
    push rdi                   
    mov rsi, rsp                
    syscall                 
    pop rax                   
    ret 


; Принимает: адрес начала буфера, размер буфера - rdi, rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале.
; Пробельные символы: пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push r13
	push r14
	push r15
	xor r13,r13  ;current length
	mov r14,rdi  ;buffer addr 
	mov r15,rsi  ;buffer length


	.loop:
	call read_char

	test rax,rax
	jz .break

	cmp rax, SPACE
	jz .space

	cmp rax, TAB
	jz .space

	cmp rax, NEW_LINE
	jz .space


	cmp r13, r15
	je .fail

	mov [r14+r13], rax
	inc r13

	jmp .loop

	.break:
		mov byte[r14+r13], 0
		mov rax, r14
		mov rdx, r13
		jmp .end

	.space:
		test r13,r13
		jz .loop
		jmp .break

	.fail:
		xor rax, rax
		xor rdx, rdx
	.end:
		pop r15
		pop r14
		pop r13
	ret






; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, в rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax               
    xor r9, r9          
    mov r8, 10
    mov rcx, -1
    xor rdx, rdx      
    .loop:
        inc rcx 
        mov r9b, [rdi+rcx]  
    
        cmp r9b, 0
    	je .end
        cmp r9b, '0'          
        jl .end                 
        cmp r9b, '9'            
        jg .end                 
        sub r9b, '0'           
        mul r8         
        add rax, r9             
        jmp .loop

    .end:
        test rcx, rcx
        jz .error
        mov rdx, rcx
        ret

    .error:
        xor rdx, rdx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'         
    je .negative_number   
    cmp byte[rdi], '+'
    je .positive_number
    jmp parse_uint

    .positive_number:
        inc rdi
        call parse_uint
        inc rdx
        jmp .end
          

    .negative_number:
        inc rdi                 
        call parse_uint
        inc rdx
        test rdx, rdx
        jz .end       
        neg rax           
        

    .end:
        ret
        





; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:                  
    xor r9, r9               
    xor rax, rax             
    xor rcx, rcx         
    .loop:
        mov r9b, byte[rdi+rcx] 
        mov byte[rsi+rcx], r9b 
        inc rcx              
        cmp rcx, rdx
        jge .overflow
        cmp r9b, 0            
        je .end                
        jmp .loop 
    .end: 
        mov rax, rcx        
        ret
    .overflow:
        mov rax, 0
        ret 


print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, WRITE_SYSCALL_NUMBER
    mov rdi, STD_ERR
    syscall
    ret
