import subprocess

sin = [
    'Car',
    '',
    'Apple',
    'ora'*150,
    'Computer',
    '   ',
    'Caar'
        ]

sout = [
    'A vehicle',
    '',
    'A fruit',
    '',
    'An electronic device',
    '',
    ''
        ]

serr = [
    '',
    'Key was not found!',
    '',
    'Error reading stdin!',
    '',
    'Key was not found!',
    'Key was not found!'
        ]

passed_tests = 0

for i in range(len(sin)):
    test_data = [sin[i], sout[i], serr[i]]

    pr = subprocess.Popen(['./program'], stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, text=True)
    stdout, stderr = pr.communicate(input=test_data[0])
    print('stdout: ',stdout,'\tstderr: ',stderr)
    stdout, stderr = stdout.strip(), stderr.strip()
    print('stdout: ',stdout,'\tstderr: ',stderr)
    print(f'Test {i + 1}:')
    print('stout:'+stdout, '.\ntest_data[1]:',  test_data[1],'.\nstderr: ', stderr, '.\ntest_data[2]:',test_data[2],'.' )
    if stdout == test_data[1] and stderr == test_data[2]:
        print('Test passed: ')
        print(f'\tSTDIN:  {test_data[0]}')
        print(f'\tSTDOUT: {test_data[1]}')
        print(f'\tSTDERR: {test_data[2]}')
        passed_tests += 1
    else:
        if stdout != test_data[1]:
            print(f'Incorrect DATA output "{stdout}" != "{test_data[1]}" for "{test_data[0]}"\n')
        if stderr != test_data[2]:
            print(f'Incorrect ERROR output "{stderr}" != "{test_data[2]}" for "{test_data[0]}"\n')

print('\nRESULTS')
print(f'\tPASSED: {passed_tests} | {len(sin)}')
print(f'\tFAILED: {len(sin) - passed_tests} | {len(sin)}')
